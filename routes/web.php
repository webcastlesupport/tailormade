<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\CatelogController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\ShopunitController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthController::class, 'showlogin'])->name('admin.showlogin');
Route::post('/login', [AuthController::class, 'login'])->name('admin.login');


Route::middleware(['admin'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');
    Route::get('/dashboard', [AuthController::class, 'dashboard'])->name('admin.dashboard');

    Route::get('/catelog', [CatelogController::class, 'view'])->name('admin.catelog');
    Route::get('/catelog/add', [CatelogController::class, 'add'])->name('admin.catelogadd');
    Route::post('/catelog/store', [CatelogController::class, 'store'])->name('admin.catelogstore');
    Route::get('/catelog/edit/{id}', [CatelogController::class, 'edit'])->name('admin.catelogedit');
    Route::post('/catelog/update/{id}', [CatelogController::class, 'update'])->name('admin.catelogupdate');

    Route::get('/banner', [BannerController::class, 'view'])->name('admin.banner');
    Route::get('/banner/add', [BannerController::class, 'add'])->name('admin.banneradd');
    Route::post('/banner/store', [BannerController::class, 'store'])->name('admin.bannerstore');
    Route::get('/banner/edit/{id}', [BannerController::class, 'edit'])->name('admin.banneredit');
    Route::post('/banner/update/{id}', [BannerController::class, 'update'])->name('admin.bannerupdate');

    Route::get('/shop', [ShopController::class, 'view'])->name('admin.shop');
    Route::get('/shop/add', [ShopController::class, 'add'])->name('admin.shopadd');
    Route::post('/shop/store', [ShopController::class, 'store'])->name('admin.shopstore');
    Route::get('/shop/edit/{id}', [ShopController::class, 'edit'])->name('admin.shopedit');
    Route::post('/shop/update/{id}', [ShopController::class, 'update'])->name('admin.shopupdate');

    Route::get('/shop/unit/{id}', [ShopunitController::class, 'view'])->name('admin.shopunit');
    Route::get('/shop/unit/add/{id}', [ShopunitController::class, 'add'])->name('admin.shopunitadd');
    Route::post('/shop/unit/store/{id}', [ShopunitController::class, 'store'])->name('admin.shopunitstore');
    Route::get('/shop/unit/edit/{id}', [ShopunitController::class, 'edit'])->name('admin.shopunitedit');
    Route::post('/shop/unit/update/{id}', [ShopunitController::class, 'update'])->name('admin.shopunitupdate');


    Route::get('/order', [OrderController::class, 'view'])->name('admin.order');
    Route::get('/order/delivery/{id}', [OrderController::class, 'delivery'])->name('admin.delivery');

    Route::get('/settings', [SettingsController::class, 'view'])->name('admin.settings');
    Route::post('/settings/store', [SettingsController::class, 'store'])->name('admin.settingsstore');
    Route::get('/user', [UserController::class, 'view'])->name('admin.user');

});
