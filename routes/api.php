<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|verifyOtp
*/


Route::post('/login', [ApiController::class, 'login'])->name('login');

Route::post('/registeration', [ApiController::class, 'registeration'])->name('registeration');

Route::post('/verifyotp', [ApiController::class, 'verifyotp'])->name('verifyotp');
Route::post('/resentotp', [ApiController::class, 'resentotp'])->name('resentotp');
Route::post('/logout', [ApiController::class, 'logout'])->name('logout');


Route::post('/addadddress', [ApiController::class, 'adddddress'])->name('adddddress');
Route::post('/editprofile', [ApiController::class, 'editprofile'])->name('editprofile');

Route::post('/search', [ApiController::class, 'search'])->name('search');

Route::post('/customercare', [ApiController::class, 'customercare'])->name('customercare');
Route::post('/about', [ApiController::class, 'about'])->name('about');

Route::post('/shoplist', [ApiController::class, 'shoplist'])->name('shoplist');
Route::post('/popularshops', [ApiController::class, 'popularshops'])->name('popularshops');
Route::post('/getcustomize', [ApiController::class, 'getcustomize'])->name('getcustomize');
Route::post('/addorder', [ApiController::class, 'addorder'])->name('addorder');
Route::post('/getorder', [ApiController::class, 'getorder'])->name('getorder');
Route::post('/changequantity', [ApiController::class, 'changequantity'])->name('changequantity');
Route::post('/getmyorder', [ApiController::class, 'getmyorder'])->name('getmyorder');
Route::post('/paymentdetails', [ApiController::class, 'paymentdetails'])->name('paymentdetails');
Route::post('/placeorder', [ApiController::class, 'placeorder'])->name('placeorder');
Route::post('/myorderlist', [ApiController::class, 'myorderlist'])->name('myorderlist');
Route::post('/getProfile', [ApiController::class, 'getProfile'])->name('getProfile');
Route::post('/updateProfile', [ApiController::class, 'updateProfile'])->name('getProfile');
