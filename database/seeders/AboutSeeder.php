<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'admin_desc' => 'test',
            'admin_phone' => '9495',
            'description' => 'test',
            'img' => '1',
            
        ]);
    }
}
