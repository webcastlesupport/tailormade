<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class shopunit extends Model
{
    use HasFactory;
    protected $table = 'shop_unit';
    public $timestamps = false;
    protected $guarded = [];
}
