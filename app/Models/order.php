<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;
    protected $table = 'order_table';
    public $timestamps = false;
    protected $guarded = [];

    public function shop(){
        return $this->hasOne(shop::class, 'id', 'shop_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
