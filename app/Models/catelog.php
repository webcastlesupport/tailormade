<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class catelog extends Model
{
    use HasFactory;
    protected $table = 'catalog';
    public $timestamps = false;
    protected $guarded = [];
}
