<?php

namespace App\Http\Controllers;

use App\Models\catelog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class CatelogController extends Controller
{
    public function view()
    {
        
        $data = catelog::get();
        return view('catelog.view',compact('data'));
    }
    public function add()
    {
        
        return view('catelog.add');
    }

    public function store(Request $request)
    {
        $rules = [
            
            'name' => 'required',
            'img' => 'required',
            'type' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.catelogadd')->withErrors($validator)->withInput();
            }
                $data = [
                    'name' => $request->name,
                    'type' => $request->type,
                     
                ];
                if ($request->hasFile('img')) {
                
                    $photo = $request->file('img')->store('toPath', ['disk' => 'public']);
        
                    $data['img'] = $photo; 
                    
        
                }
            catelog::create($data);
            return redirect()->route('admin.catelog');
    }

    public function edit($id)
    {
        $d = catelog::where('id',$id)->first();
        return view('catelog.edit',compact('d'));
    }

    public function update(Request $request,$id)
    {
        $rules = [
            
            'name' => 'required',
            
            'type' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.catelogedit',$id)->withErrors($validator)->withInput();
            }
                $data = [
                    'name' => $request->name,
                    'type' => $request->type,
                     
                ];
                if ($request->hasFile('img')) {
                
                    $photo = $request->file('img')->store('toPath', ['disk' => 'public']);
        
                    $data['img'] = $photo; 
                    
        
                }
            catelog::where('id',$id)->update($data);
            return redirect()->route('admin.catelog');
    }
}
