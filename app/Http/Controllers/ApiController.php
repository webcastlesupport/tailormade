<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\catelog;
use App\Models\client;
use App\Models\order;
use App\Models\settings;
use App\Models\shop;
use App\Models\shopunit;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function apierror()
    {
        return response()->json([
            "ErrorCode" => 1,
            "Data" => (object)[],
            "Message" => 'Error'
        ]); 
    }

    public function login(Request $request)
    {
        $validator      =       Validator::make($request->all(),
    [
        
        'phone'      =>     'required',
        'device_type'   =>      'required',
        'device_token'  =>      'required'
    ]
    );

        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

        $otp = rand(1000,9999);
        $user = User::where('phone',$request->phone)->first();

        if($user)
        {
            $data = [
                'otp' => $otp,
                'otp_time' => Carbon::now(),
                'password' => Hash::make($otp)
            ];
            User::where('id',$user->id)->update($data);
            return response()->json([

                "ErrorCode" => 0,
                "Data" => array(
                    "user_id" => $user->id,
                    "otp" => $otp,
                    "phone" => $user->phone,
                ),
                "Message" => "Success"
                
            ]);
        }
        else{
            
            $data = [
                'otp' => $otp,
                'otp_time' => Carbon::now(),
                'password' => Hash::make($otp),
                'phone' => $request->phone,
                'device_type' => $request->device_type,
                'device_token'=> $request->device_token,
                'is_active' => '0'
            ];
            
            $newuser = User::create($data);
            
            return response()->json([

                "ErrorCode" => 0,
                "Data" => array(
                    "user_id" => $newuser->id,
                    "otp" => $newuser->otp,
                    "phone" => $newuser->phone,
                ),
                "Message" => "Success"
                
            ]);
        }
    }

   public function registeration(Request $request)
   {
    $validator      =       Validator::make($request->all(),
    [
        'email'         =>     'required|email|unique:users,email',
        'phone'      =>     'required',
        'first_name'      =>     'required',
        'last_name'      =>     'required',
        'device_type'   =>      'required',
        'device_token'  =>      'required'
    ]
    );

        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }
        $data = [
            'email' => $request->email,
            'phone' => $request->phone,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'device_type' => $request->device_type,
            'device_token' => $request->device_token,
            'is_active' => '1'
        ];

       $user = User::where('phone',$request->phone)->first();
       if($user)
       {
            User::where('id',$user->id)->update($data);
            return response()->json([
                "ErrorCode" => 0,
                "Data" => (object)[],
                "Message" => "Success"
                
            ]);
       }
       else{
        return response()->json([
            "ErrorCode" => 1,
            "Data" => (object)[],
            "Message" => "user not found"
            
        ]);
       }

   }

   

   public function resentotp(Request $request)
   {
    $validator      =       Validator::make($request->all(),
[
    
    'phone'      =>     'required',
    'device_type'   =>      'required',
    'device_token'  =>      'required'
]
);

    if($validator->fails()) {
        return response()->json(["validation_errors" => $validator->errors()]);
    }

    $otp = rand(1000,9999);
    $user = User::where('phone',$request->phone)->first();

    if($user)
    {
        $data = [
            'otp' => $otp,
            'otp_time' => Carbon::now(),
            'password' => Hash::make($otp)
        ];
        User::where('id',$user->id)->update($data);
        return response()->json([

            "ErrorCode" => 0,
            "Data" => array(
                "user_id" => $user->id,
                "otp" => $otp,
                "phone" => $user->phone,
            ),
            "Message" => "Success"
            
        ]);
    }
    else{
        
       
        
        return response()->json([

            "ErrorCode" => 1,
            "Data" => array(
                ),
            "Message" => "user not found"
            
        ]);
    }
}

public function editprofile(Request $request)
   {
    $validator = Validator::make($request->all(),
[
    
    'user_id'=>'required',
    'name'=>'required',
    'profile_img'=>'required'
]
);

    if($validator->fails()) {
        return response()->json(["validation_errors" => $validator->errors()]);
    }

   
    $user = User::where('id',$request->user_id)->first();

    if($user)
    {
        $data = [
            'name' => $request->name,
            
        ];

        if ($request->hasFile('profile_img')) {
                
            $photo = $request->file('profile_img')->store('toPath', ['disk' => 'public']);
    
            $data['profile_img'] = $photo; 
            
    
        }

        User::where('id',$user->id)->update($data);
        return response()->json([

            "ErrorCode" => 0,
            "Data" => array(
                
            ),
            "Message" => "Success"
            
        ]);
    }
    else{
        
       
        
        return response()->json([

            "ErrorCode" => 1,
            "Data" => array(
                ),
            "Message" => "user not found"
            
        ]);
    }
}

public function search(Request $request)
{
    $validator = Validator::make($request->all(),
    [
        
        'search'=>'required',
        
    ]
    );
    
        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

        $shops = shop::where('active','1')->where('name', 'LIKE', "%$request->search%")->get();
        $result = array();
        foreach($shops as $s)
        {
         $result[] = array(
             'shop_id' => $s->id,
             'shop_name' => $s->name,
             'time' => $s->time,
             'shop_img' => url('/').'/'.$s->img,
             'delivery_charge' => $s->delivery_charge,
         ); 
        }
 
        return response()->json([
         "ErrorCode" => 0,
         "Data" => 
             $result
             
         ,
         "Message" => "Success"
         
     ]);
 
}

public function adddddress(Request $request)
{
    $validator      =       Validator::make($request->all(),
    [
        
        'user_id'=>'required',
        'latitude'=>'required',
        'longitude'=>'required',
        'device_token'=>'required',
        'device_type'=> 'required',
        'name'=>'required',
        'mobile'=>'required',
        'address'=> 'required',
        'address_type'=>'required', 
    ]
    );

        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

        $user = User::where('id',$request->user_id)->first();

        if($user)
        {
            $data = [
                'device_type' => $request->device_type,

                'device_token' => $request->device_token,
                'lat' => $request->latitude,
                'longi' => $request->longitude,
                'name' => $request->name,
                'mobile' => $request->mobile,
                'address' => $request->address,
                'address_type' => $request->address_type,
            ];
            User::where('id',$request->user_id)->update($data);

            return response()->json([
    
                "ErrorCode" => 0,
                "Data" => array(
                    'address_line'=> $request->address
                    ),
                "Message" => "Success"
                
            ]);

        }
        else{
        
       
        
            return response()->json([
    
                "ErrorCode" => 1,
                "Data" => array(
                    ),
                "Message" => "user not found"
                
            ]);
        }
}

public function logout(Request $request)
{
    $validator      =       Validator::make($request->all(),
[
    
    'user_id'      =>     'required',
    
]
);

    if($validator->fails()) {
        return response()->json(["validation_errors" => $validator->errors()]);
    }
    
    $userToLogout = User::find($request->user_id);
    Auth::setUser($userToLogout);
     Auth::logout();
    return response()->json([
    
        "ErrorCode" => 0,
        "Data" => array(
            ),
        "Message" => "Success"
        
    ]);
}
   public function verifyotp(Request $request)
   {
    $validator      =       Validator::make($request->all(),
    [
        
        'user_id'=> 'required',
        'otp'   =>      'required',
        
    ]
    );

        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

     $client = User::where('id',$request->user_id)->first();
     
     if($client)
     {
        if(Auth::attempt(['id' => $request->user_id, 'password' => $request->otp]))
        {
            
            return response()->json([

                "ErrorCode" => 0,
                "Data" => array(
                    "user_id" => $client->id,
                    "isLogin" => $client->is_active,
                    "access_token" => $client->device_token,
                    "name" => $client->name,
                    "email" => $client->email,
                    "mobile" => $client->mobile,
                    "address" => $client->address,
                    "longitude" => $client->longi,
                    "latitude" => $client->lat,
                    "access_token" => $client->device_token
                ),
                "Message" => "Success"
                
            ]);
        }     
        else{
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "incorrect otp"
                
            ]);
        }
         
     }
     else{
        return response()->json([
            "ErrorCode" => 1,
            "Data" => (object)[],
            "Message" => "User not found"
            
        ]);
     }
   }

   public function shoplist()
   {
       $shops = shop::where('active','1')->get();
       $result = array();
       foreach($shops as $s)
       {
        $result[] = array(
            'shop_id' => $s->id,
            'shop_name' => $s->name,
            'time' => $s->time,
            'shop_img' => url('/').'/'.$s->img,
            'delivery_charge' => $s->delivery_charge,
        ); 
       }

       return response()->json([
        "ErrorCode" => 0,
        "Data" => 
            $result
            
        ,
        "Message" => "Success"
        
    ]);

       
   }

   public function popularshops()
   {
       $shops = shop::where('active','1')->where('popular','1')->get();
       $result = array();
       foreach($shops as $s)
       {
        $result[] = array(
            'shop_id' => $s->id,
            'shop_name' => $s->name,
            'time' => $s->time,
            'shop_img' => url('/').'/'.$s->img,
            'delivery_charge' => $s->delivery_charge,
        ); 
       }

       return response()->json([
        "ErrorCode" => 0,
        "Data" => 
            $result
            
        ,
        "Message" => "Success"
        
    ]);

       
   }

   public function getcustomize(Request $request)
   {
    $rules = [
            
        'shop_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }

    $shop = shop::where('id',$request->shop_id)->first();
    $unit = shopunit::where('shop_id',$request->shop_id)->get();
    $catlog = json_decode($shop->catalouge);
    $ctop = catelog::whereIn('id',$catlog)->where('type','Top')->get();
    $cbottom = catelog::whereIn('id',$catlog)->where('type','Bottom')->get();
    $result = array();
    $units = array();

    $catop = array();
    $cabottom = array();
    
    foreach($ctop as $a)
    {
     $catop[] = array(
         'item_id' => $a->id,
         'item_name' => $a->name,
         'item_img' => url('/').'/'.$a->img,
     ); 
    }
    foreach($cbottom as $a)
    {
     $cabottom[] = array(
         'item_id' => $a->id,
         'item_name' => $a->name,
         'item_img' => url('/').'/'.$a->img,
     ); 
    }
    
    foreach($unit as $u)
    {
     $units[] = array(
         'unit_name' => $u->unit,
     ); 
    }

    
     $result[] = array(
         'shop_name' => $shop->name,
         'shop_address' => $shop->address,
         'shop_whatsapp' => $shop->whats_app,
         'delivery_charge' => $shop->delivery_charge,
         'units' => $units,
         'category_styles_top' =>$catop,
         'category_styles_bottom' =>$cabottom 
     ); 
    
     return response()->json([
        "ErrorCode" => 0,
        "Data" => 
            $result
            
        ,
        "Message" => "Success"
        
    ]);

   }
public function addorder(Request $request)
{
    $rules = [
            
        'shop_id' => 'required',
        'user_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $order_id = 'ord'.rand();
        
        /////rate top///

        $unit_top = $request->unit_top;
        $unit_top = shopunit::where('unit',$request->unit_top)->where('shop_id',$request->shop_id)->first();
        
        $top_price = $unit_top->price?? 0;
        $total_top_length = $request->chest_measure + $request->shoulder_measure + $request->length_measure_top + $request->nick_measure + $request->wrist_measure + $request->arm_measure + $request->waist_measure_top;
        $rate_top = $top_price * $total_top_length;


        //////
        $unit_bottom = $request->unit_bottom;
        $unit_bottom = shopunit::where('unit',$request->unit_bottom)->where('shop_id',$request->shop_id)->first();
        $bottom_price = $unit_bottom->price ?? 0;
        $total_bottom_length = $request->waist_measure_bottom + $request->hip_measure + $request->length_measure_bottom + $request->front_measure + $request->insram_measure + $request->tights_measure + $request->leg_measure;
        $rate_bottom = $bottom_price * $total_bottom_length;

        $rate = $rate_top + $rate_bottom;



    $data = [
        'order_id' => $order_id,
        'user_id' => $request->user_id,
        'shop_id' => $request->shop_id,
        'completed' => '0',
        'date' => Carbon::now(),
        'rate' => $rate,
        'rate_per_quantity' => $rate,
        'quantity' => $request->quantity ?? 1,
        'catalouge_id_top' => $request->catalouge_id_top ?? null,
        'catalouge_id_bottom' => $request->catalouge_id_bottom ?? null,
        'unit_top' => $request->unit_top ?? null,
        'unit_bottom' => $request->unit_bottom ?? null,
        'chest_measure' => $request->chest_measure ?? null,
        'shoulder_measure' => $request->shoulder_measure ?? null,
        'length_measure_top' => $request->length_measure_top ?? null,
        'nick_measure' => $request->nick_measure ?? null,
        'waist_measure_top' => $request->waist_measure_top ?? null,
        'arm_measure' => $request->arm_measure ?? null,
        'color_top' => $request->color_top ?? null,
        'color_bottom' => $request->color_bottom ?? null,
        'material_top' => $request->material_top ?? null,
        'material_bottom' => $request->material_bottom ?? null,

        'multipart_image_top' => $multipart_image_top ?? null,
        'multipart_image_bottom' => $multipart_image_bottom ?? null,
        'waist_measure_bottom' => $request->waist_measure_bottom ?? null,
        'hip_measure' => $request->hip_measure ?? null,
        'length_measure_bottom' => $request->length_measure_bottom ?? null,
        'front_measure' => $request->front_measure ?? null,
        'insram_measure' => $request->insram_measure ?? null,
        'tights_measure' => $request->tights_measure ?? null,
        'leg_measure' => $request->leg_measure ?? null,
        
    ];

    if ($request->hasFile('multipart_image_top')) {
                
        $photo = $request->file('multipart_image_top')->store('toPath', ['disk' => 'public']);

        $data['multipart_image_top'] = $photo; 
        

    }
    if ($request->hasFile('multipart_image_bottom')) {
                
        $photo = $request->file('multipart_image_bottom')->store('toPath', ['disk' => 'public']);

        $data['multipart_image_bottom'] = $photo; 
        

    }
    $new_order = order::create($data);
    $result = array(
        'id' => $new_order->id,

        'order_id' => $new_order->order_id, 
    );
    return response()->json([
        "ErrorCode" => 0,
        "Data" => 
            $result
            
        ,
        "Message" => "Success"
        
    ]);
}

public function getorder(Request $request)
{
    $rules = [
            
        'order_id' => 'required',
        'user_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $order = order::where('id',$request->order_id)->first();
        if($order)
        {
        $result = array(
            'id' => $order->id,
            'order_id' => $order->order_id,
            'cost' => $order->rate,
            'product_img_top' => url('/').'/'.$order->multipart_image_top,
            'product_img_bottom' => url('/').'/'.$order->multipart_image_bottom,
            'details' => 'order cost is around '.$order->rate.' rs with the material '.$order->material_top.' and '.$order->material_bottom.'.'
        );

        return response()->json([
            "ErrorCode" => 0,
            "Data" => 
                $result
                
            ,
            "Message" => "Success"
            
        ]);
    }
    else{
        return response()->json([
            "ErrorCode" => 1,
            "Data" => (object)[],
            "Message" => "order not found"
        ]);
    }
}
public function changequantity(Request $request)
{
    $rules = [
            
        'order_id' => 'required',
        'quantity' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $order = order::where('id',$request->order_id)->first();
        if($order)
        {
            $ratePerQuantity = $order->rate_per_quantity;

            $data = [
                'rate' => $ratePerQuantity * $request->quantity,
                'quantity' => $request->quantity
            ];
            order::where('id',$order->id)->update($data);

            $result = array(
                'quantity' => $request->quantity,
                
            );

            return response()->json([
                "ErrorCode" => 0,
                "Data" => 
                    $result
                    
                ,
                "Message" => "Success"
                
            ]);

        }
        else{
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "order not found"
            ]);
        }
}

public function getmyorder(Request $request)
{
    $rules = [
            
        'order_id' => 'required',
        'user_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $order = order::where('id',$request->order_id)->first();
        if($order)
        {
            $ban = Banner::get();
            $banner = array();
            foreach($ban as $b)
            {
                $banner[] = array(
                    'title' => $b->title,
                    'desc' => $b->desc,
                    'img' => url('/').'/'.$b->logo
                );
            }
            $orderr = array(
                'id' => $order->id,
                'order_id' => $order->order_id,
                'price' => $order->rate,
                'quantity' => $order->quantity,
                'item_img_top' => url('/').'/'.$order->multipart_image_top ?? null,
                'item_img_bottom' => url('/').'/'.$order->multipart_image_bottom ?? null,
            );
            $result = array(
                'sub_total' => $order->rate,
                'shipping_charge' =>$order->shop->delivery_charge,
                'grand_total' => $order->rate + $order->shop->delivery_charge,
                'banner' => $banner,
                'ordered_items' => $orderr
                
            );

            return response()->json([
                "ErrorCode" => 0,
                "Data" => 
                    $result
                    
                ,
                "Message" => "Success"
                
            ]);
        }
        else{
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "order not found"
            ]);
        }
}

public function paymentdetails(Request $request)
{
    $rules = [
            
        'order_id' => 'required',
        'user_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $order = order::where('id',$request->order_id)->first();
        if($order)
        {
            $category_img_top = array();
            $category_img_bottom = array();
            if($order->catalouge_id_top != null)
            {
                $catalouge_img_top = json_decode($order->catalouge_id_top);
                foreach($catalouge_img_top as $cimg)
                {
                    $cat = catelog::where('id',$cimg)->first();
                    if($cat) {
                        $category_img_top[] = array(
                            'top_img' => url('/').'/'.$cat->img
                        );
                    }
                    
                }
            }
            if($order->catalouge_id_bottom != null)
            {
                $catalouge_img_bottom = json_decode($order->catalouge_id_bottom);
                foreach($catalouge_img_bottom as $cimg)
                {
                    $cat = catelog::where('id',$cimg)->first();
                    if($cat) {
                        $category_img_bottom[] = array(
                            'bottom_img' => url('/').'/'.$cat->img
                        );
                    }
                    
                }
            }
            $result = array(
                'id' => $order->id,
                'order_id' => $order->order_id,
                'price' => $order->rate,
                'quantity' => $order->quantity,
                'item_img_top' => url('/').'/'.$order->multipart_image_top ?? null,
                'item_img_bottom' => url('/').'/'.$order->multipart_image_bottom ?? null,
                'top' => $category_img_top ?? null,
                'bottom' =>$category_img_bottom ?? null,
                'chest_measure' => $order->chest_measure ?? null,
                'shoulder_measure' => $order->shoulder_measure ?? null,
                'length_measure_top' => $order->length_measure_top ?? null,
                'nick_measure' => $order->nick_measure ?? null,
                'waist_measure_top' => $order->waist_measure_top ?? null,
                'arm_measure' => $order->arm_measure ?? null,
                'color_top' => $order->color_top ?? null,
                'color_bottom' => $order->color_bottom ?? null,
                'material_top' => $order->material_top ?? null,
                'material_bottom' => $order->material_bottom ?? null,
                'waist_measure_bottom' => $order->waist_measure_bottom ?? null,
                'hip_measure' => $order->hip_measure ?? null,

                'length_measure_bottom' => $order->length_measure_bottom ?? null,
                'front_measure' => $order->front_measure ?? null,
                'insram_measure' => $order->insram_measure ?? null,
                'tights_measure' => $order->tights_measure ?? null,
                'leg_measure' => $order->leg_measure ?? null,
                

                
            );

            return response()->json([
                "ErrorCode" => 0,
                "Data" => 
                    $result
                    
                ,
                "Message" => "Success"
                
            ]);

        }
        else{
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "order not found"
            ]);
        }
}

public function placeorder(Request $request)
{
    $rules = [
            
        'order_id' => 'required',
        'user_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $order = order::where('id',$request->order_id)->first();
        if($order)
        {
            $data = [
                'completed' => '1'
            ];
            order::where('id',$request->order_id)->update($data);

            return response()->json([
                "ErrorCode" => 0,
                "Data" => '',
                "Message" => 'Success'
            ]);
        }
        else{
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "order not found"
            ]);
        }
}

public function myorderlist(Request $request)
{
    $rules = [
            
        
        'user_id' => 'required',
        
        
    ];

    $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => $validator->errors()
            ]);
        
           
        }
        $orderr = order::where('user_id',$request->user_id)->get();
        if(count($orderr))
        {
            foreach($orderr as $order )
            $result[] = array(
                'id' => $order->id,
                'order_id' => $order->order_id,
                'delivered_date' =>'',
                'date' => $order->date,
                'item_img_top' => url('/').'/'.$order->multipart_image_top ?? null,
                'item_img_bottom' => url('/').'/'.$order->multipart_image_bottom ?? null,
                
            );

            return response()->json([
                "ErrorCode" => 0,
                "Data" => 
                    $result
                    
                ,
                "Message" => "Success"
                
            ]);
        }
        else{
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "order not found"
            ]);
        }
}

public function customercare()
{
    $set = settings::where('id',1)->first();
    $result[] = array(
        'admin_desc' => $set->admin_desc,
        'admin_phone' => $set->admin_phone,
    );

    return response()->json([
        "ErrorCode" => 0,
        "Data" => 
            $result
            
        ,
        "Message" => "Success"
        
    ]);
}

public function about()
{
    $set = settings::where('id',1)->first();
    $result[] = array(
        'description' => $set->description,
        'img' => url('/').'/'.$set->img,
    );

    return response()->json([
        "ErrorCode" => 0,
        "Data" => 
            $result
            
        ,
        "Message" => "Success"
        
    ]);
}
    public function getProfile(Request $request) 
    {
        $rules = [
            'user_id' => 'required',
        ];
    
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

        $user = User::find($request->user_id);

        if($user) {

            $result = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'address' => $user->address,
                'latitude' => $user->lat,
                'longitude' => $user->longi,
            ];

            return response()->json([
                "ErrorCode" => 0,
                "Data" => $result,
                "Message" => "Success"
                
            ]);
        } else {
           
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "user not found"
            ]);
           
        }
    }

    public function updateProfile(Request $request) 
    {
        $rules = [
            'user_id' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->user_id
        ];
    
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json(["validation_errors" => $validator->errors()]);
        }

        $user = User::find($request->user_id);

        if($user) {

            $user->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);

            $result = [
                'name' => $user->name,
                'email' => $user->email,
            ];

            return response()->json([
                "ErrorCode" => 0,
                "Data" => $result,
                "Message" => "Updated Successfully"
                
            ]);
        } else {
           
            return response()->json([
                "ErrorCode" => 1,
                "Data" => (object)[],
                "Message" => "user not found"
            ]);
           
        }
    }

}
