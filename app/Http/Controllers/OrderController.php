<?php

namespace App\Http\Controllers;

use App\Models\order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function view()
    {
        $data = order::get();
        return view('order.view',compact('data'));
    }

    public function delivery($id)
    {
        $data = [
            'delivery_date' => Carbon::now()
        ];
        order::where('id',$id)->update($data);

        return redirect()->route('admin.order');
    }
}
