<?php

namespace App\Http\Controllers;

use App\Models\order;
use App\Models\shop;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{

    public function showlogin()
    {
        
        return view('login');
    }

    public function login(Request $request)
    {
        $messages = [
            'email'=>'Field Required',
            'password'=>'Field Required',
            ];       
        $rules = array(
            'email'    => 'required|email', 
            'password' => 'required|alphaNum|min:3' 
        );

        
        $validator = Validator::make($request->all(), $rules,$messages);

        
        if ($validator->fails()) {
            Session::flash('message', 'Please correct the errors and re-submit');
            Session::flash('alert-class', 'bg-warning');
            return redirect()->route('admin.showlogin')->withErrors($validator)->withInput();
        } 
        else
        {
            $credentials = $request->only('email', 'password');

            
            if (Auth::guard('webadmin')->attempt($credentials)) 
            {

                
                return redirect()->route('admin.dashboard');

            } 
            else 
            {

                Session::flash('message', 'uid or password is incorrect');
                Session::flash('alert-class', 'bg-warning');
                return redirect()->route('admin.showlogin');

            }
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.showlogin');
    }

    public function dashboard()
    {
       $user = User::count();
       $shop = shop::count();
       $order = order::count();
        return view('dashboard',compact('user','shop','order'));
    }
}
