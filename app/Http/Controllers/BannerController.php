<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class BannerController extends Controller
{
    public function view()
    {
        
        $data = Banner::get();
        return view('banner.view',compact('data'));
    }
    public function add()
    {
        
        return view('banner.add');
    }

    public function store(Request $request)
    {
        $rules = [
            
            'title' => 'required',
            'logo' => 'required',
            'desc' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.banneradd')->withErrors($validator)->withInput();
            }
                $data = [
                    'title' => $request->title,
                    'desc' => $request->desc,
                     
                ];
                if ($request->hasFile('logo')) {
                
                    $photo = $request->file('logo')->store('toPath', ['disk' => 'public']);
        
                    $data['logo'] = $photo; 
                    
        
                }
            Banner::create($data);
            return redirect()->route('admin.banner');
    }

    public function edit($id)
    {
        $d = Banner::where('id',$id)->first();
        return view('banner.edit',compact('d'));
    }

    public function update(Request $request,$id)
    {
        $rules = [
            
            'title' => 'required',
            
            'desc' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.banneredit',$id)->withErrors($validator)->withInput();
            }
                $data = [
                    'title' => $request->title,
                    'desc' => $request->desc,
                     
                ];
                if ($request->hasFile('logo')) {
                
                    $photo = $request->file('logo')->store('toPath', ['disk' => 'public']);
        
                    $data['logo'] = $photo; 
                    
        
                }
            Banner::where('id',$id)->update($data);
            return redirect()->route('admin.banner');
    }
}
