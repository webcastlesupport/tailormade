<?php

namespace App\Http\Controllers;

use App\Models\catelog;
use App\Models\shop;
use App\Models\shopunit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function view()
    {
        $data = shop::get();
        return view('shop.view',compact('data'));
    }
    public function add()
    {
        $cat = catelog::get();
        return view('shop.add',compact('cat'));
    }

    public function store(Request $request)
    {
        
        $rules = [
            
            'name' => 'required',
            'img' => 'required',
            'time' => 'required',
            'delivery_charge' => 'required',
            'active' => 'required',
            'address' => 'required',
            'whats_app' => 'required',
            'catalouge' => 'required',
            'popular' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
       
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.shopadd')->withErrors($validator)->withInput();
            }
            
                $data = [
                    'name' => $request->name,
                    'time' => $request->time,
                    'delivery_charge' => $request->delivery_charge,
                    'active' => $request->active,
                    'address' => $request->address,
                    'whats_app' => $request->whats_app,
                    'catalouge' => json_encode($request->catalouge),
                    'popular' => $request->popular,
                     
                ];
                if ($request->hasFile('img')) {
                
                    $photo = $request->file('img')->store('toPath', ['disk' => 'public']);
        
                    $data['img'] = $photo; 
                    
        
                }
            $sho = shop::create($data);

            $data = [
                'shop_id' => $sho->id,
                'unit' => 'meter',
                'price' => 1,
                'default' => '1'
            ];

            shopunit::create($data);
            
            return redirect()->route('admin.shop');
    }

    public function edit($id)
    {
        $cat = catelog::get();
        $d = shop::where('id',$id)->first();
        
        return view('shop.edit',compact('d','cat'));
    }

    public function update(Request $request,$id)
    {
        $rules = [
            
            'name' => 'required',
            
            'time' => 'required',
            'delivery_charge' => 'required',
            'active' => 'required',
            'address' => 'required',
            'whats_app' => 'required',
            'catalouge' => 'required',
            'popular' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
       
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.shopedit',$id)->withErrors($validator)->withInput();
            }
            
                $data = [
                    'name' => $request->name,
                    'time' => $request->time,
                    'delivery_charge' => $request->delivery_charge,
                    'active' => $request->active,
                    'address' => $request->address,
                    'whats_app' => $request->whats_app,
                    'catalouge' => json_encode($request->catalouge),
                    'popular' => $request->popular,
                     
                ];
                if ($request->hasFile('img')) {
                
                    $photo = $request->file('img')->store('toPath', ['disk' => 'public']);
        
                    $data['img'] = $photo; 
                    
        
                }
            shop::where('id',$id)->update($data);
            
            return redirect()->route('admin.shop');
    }
}
