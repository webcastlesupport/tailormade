<?php

namespace App\Http\Controllers;

use App\Models\shopunit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class ShopunitController extends Controller
{
    public function view($id)
    {
        $data = shopunit::where('shop_id',$id)->get();
        return view('shopunit.view',compact('data','id'));
    }

    public function add($id)
    {
        
        return view('shopunit.add',compact('id'));
    }

    public function store(Request $request,$id)
    {
        $rules = [
            
            'unit' => 'required',
            'price' => 'required',
            
        ];

        $validator = Validator::make($request->all(), $rules);
       
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.shopunitadd',$id)->withErrors($validator)->withInput();
            }
            
                $data = [
                    'shop_id' => $id,
                    'unit' => $request->unit,
                    'price' => $request->price,  
                    'default' => 0,
                ];
                
            shopunit::create($data);

            return redirect()->route('admin.shopunit',$id);
    }

    public function edit($id)
    {
        $d = shopunit::where('id',$id)->first();
        return view('shopunit.edit',compact('d','id'));
    }

    public function update(Request $request,$id)
    {
        $rules = [
            
            'unit' => 'required',
            'price' => 'required',
            
        ];

        $validator = Validator::make($request->all(), $rules);
       
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.shopunitedit',$id)->withErrors($validator)->withInput();
            }
            
                $data = [
                    
                    'unit' => $request->unit,
                    'price' => $request->price,  
                    'default' => 0,
                ];
                
           $sho =  shopunit::where('id',$id)->update($data);
           $sho =  shopunit::where('id',$id)->first();
            return redirect()->route('admin.shopunit',$sho->shop_id);
    }
}
