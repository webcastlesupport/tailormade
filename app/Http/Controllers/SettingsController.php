<?php

namespace App\Http\Controllers;

use App\Models\settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class SettingsController extends Controller
{
    public function view()
    {
        $d = settings::where('id','1')->first();
        return view('settings.view',compact('d'));
    }

    public function store(Request $request)
    {
        $rules = [
            
            'admin_desc' => 'required',
            
            'admin_phone' => 'required',
            'description' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        
            if ($validator->fails()) {
    
                Session::flash('message', 'Please correct the errors and re-submit');
                Session::flash('alert-class', 'bg-danger');
                
                return redirect()->route('admin.settings')->withErrors($validator)->withInput();
            }
            $data = [
                'admin_desc' => $request->admin_desc,
                'admin_phone' => $request->admin_phone,
                'description' => $request->description,
                 
            ];
            if ($request->hasFile('img')) {
            
                $photo = $request->file('img')->store('toPath', ['disk' => 'public']);
    
                $data['img'] = $photo; 
                
    
            }
        settings::where('id',1)->update($data);
        return redirect()->route('admin.settings');
    }
}
