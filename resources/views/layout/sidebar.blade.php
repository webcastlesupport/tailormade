<div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="{{route('admin.dashboard')}}"><img src="{{asset('front/images/footer-logo.png')}}" alt="Logo" srcset=""></a>
                        </div>
                        <div class="toggler">
                            <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>
                       
                        <li class="sidebar-item {{Request::is('admin/dashboard') ? 'active' : '' }}" >
                            <a href="{{route('admin.dashboard')}}" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Dashboard</span>
                            </a>
                        </li> 
                        <li class="sidebar-item {{Request::is('admin/order') ? 'active' : '' }}" >
                            <a href="{{route('admin.order')}}" class='sidebar-link'>
                                <i class="bi bi-cash"></i>
                                <span>Orders</span>
                            </a>
                        </li> 

                        <li class="sidebar-item {{Request::is('admin/user') ? 'active' : '' }}" >
                            <a href="{{route('admin.user')}}" class='sidebar-link'>
                                <i class="bi bi-person"></i>
                                <span>Users</span>
                            </a>
                        </li> 
                        <li class="sidebar-item {{Request::is('admin/settings') ? 'active' : '' }}" >
                            <a href="{{route('admin.settings')}}" class='sidebar-link'>
                                <i class="bi bi-gear"></i>
                                <span>Settings</span>
                            </a>
                        </li> 
                        

                        <li class="sidebar-item  has-sub {{Request::is('admin/homepage') ? 'active' : '' }} {{Request::is('admin/homepage/slider') ? 'active' : '' }}">
                            <a href="#" class="sidebar-link ">
                                <i class="bi bi-house-door"></i>
                                <span>Shops</span>
                            </a>
                            <ul class="submenu ">
                            
                                <li class="submenu-item {{Request::is('admin/homepage') ? 'active' : '' }}">
                                    <a href="{{route('admin.shop')}}">Shop</a>
                                </li>
                            
                                <li class="submenu-item {{Request::is('admin/homepage/slider') ? 'active' : '' }}">
                                    <a href="{{route('admin.catelog')}}">Catalogue</a>
                                </li>
                                <li class="submenu-item {{Request::is('admin/homepage/slider') ? 'active' : '' }}">
                                    <a href="{{route('admin.banner')}}">Banner</a>
                                </li>
                              
                            </ul>
                        </li>

                        

             

                       

                        

                        

                     

                        

                        

                        

                        

                       

                        

                        

                       

                        

                        

                       

                        

                        

                        

                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>