@extends('layout.master')
@section('title') Dashboard | TailorMade @endsection
@section('content_title') Dashboard @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="#">Dashboard</a></li>

@endsection

@section('content')
<div class="row">
        <div class="col-sm-3">
            <div class="card">
                <img class="card-img-top img-fluid" src="https://png.pngtree.com/png-clipart/20190516/original/pngtree-businessman-vector-icon-png-image_3710727.jpg" alt="Card image cap">
                <div class="card-block">
                    <h4 class="card-title">Total Customers</h4>
                    <p class="card-text">{{$user}}</p>
                    
                </div>
            </div>
        </div>
        <div class="col-sm-3">
        <img class="card-img-top img-fluid" src="https://png.pngtree.com/png-clipart/20190614/original/pngtree-vector-shop-icon-png-image_3788233.jpg" alt="Card image cap">
            <div class="card">
                <div class="card-block">
                <h4 class="card-title">Total Shops</h4>
                    <p class="card-text">{{$shop}}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
        <img class="card-img-top img-fluid" src="https://png.pngtree.com/png-clipart/20190629/original/pngtree-vector-money-bags-icon-png-image_4090508.jpg" alt="Card image cap">
            <div class="card">
                <div class="card-block">
                <h4 class="card-title">Total Orders</h4>
                    <p class="card-text">{{$order}}</p>
                </div>
            </div>
        </div>
</div>
@endsection