@extends('layout.master')
@section('title') Settings | Notech @endsection
@section('content_title') Settings @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="#">Settings</a></li>

@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit Settings</h4>
            </div>
        <div class="card-body">
            <section id="multiple-column-form">
                <div class="row match-height">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" method="POST" action="{{route('admin.settingsstore')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                        <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Main Description</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="admin_desc" value="{{$d->admin_desc}}">
                                                    </div>
                                                    @if ($errors->has('admin_desc'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('admin_desc') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Admin Phone</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="admin_phone" value="{{$d->admin_phone}}">
                                                    </div>
                                                    @if ($errors->has('admin_phone'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('admin_phone') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Description</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="description" value="{{$d->description}}">
                                                    </div>
                                                    @if ($errors->has('description'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('description') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                                

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Image</label>
                                                        <input type="file" id="country-floating" class="form-control"
                                                            name="img" placeholder="Country">
                                                    </div>
                                                    <div class="mt-3">
                                                    <img width="75" height="75" src="{{asset('/'.$d->img)}}" alt="">
                                        
                                                 </div>
                                                 @if ($errors->has('img'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('img') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                               
                                                
                                               
                                               

                                            


                                           
 
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit"
                                                        class="btn btn-primary me-1 mb-1">Submit</button>
                                                    <button type="reset"
                                                        class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>



@endsection