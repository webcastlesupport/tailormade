@extends('layout.master')
@section('title') Catalogue | TailorMade @endsection
@section('content_title') Catalogue @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="#">View Catalogue</a></li>
<!-- <li class="breadcrumb-item active" aria-current="page">Layout Vertical Navbar</li> -->
@endsection


@section('content')
<section class="section">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-primary rounded-pill" href="{{route('admin.catelogadd')}}" style="float: right;">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped" id="table1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    @foreach($data as $key=>$d)
                                        <td>{{$key+1}}</td>
                                        
                                        <td>{{$d->name}}</td>
                                        <td>{{$d->type}}</td>
                                        
                                          <td><img width="75" height="75" src="{{asset('/'.$d->img)}}" alt="">
                                        </td>
                                        
                                        
                                        <td>
                                        <a href="{{route('admin.catelogedit',$d->id)}}"><span class="btn btn-primary rounded-pill">Edit</span></a>
                                        </td>
                                    </tr>

                                    @endforeach
                                   
                                    
                                   

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <script src="{{asset('assets/vendors/simple-datatables/simple-datatables.js')}}"></script>
     <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

@endsection