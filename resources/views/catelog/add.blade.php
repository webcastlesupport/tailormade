@extends('layout.master')
@section('title') Catalogue | Notech @endsection
@section('content_title') Catalogue @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="{{route('admin.catelog')}}">Catalogue</a></li>
<li class="breadcrumb-item active">Add</li>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Add Catalogue</h4>
            </div>
        <div class="card-body">
            <section id="multiple-column-form">
                <div class="row match-height">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" method="POST" action="{{route('admin.catelogstore')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                        <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Title</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="name">
                                                    </div>
                                                    @if ($errors->has('name'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('name') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                                

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Image</label>
                                                        <input type="file" id="country-floating" class="form-control"
                                                            name="img" placeholder="Country">
                                                    </div>
                                                    <!-- <div class="mt-3">
                                                    <img width="75" height="75" src="https://s414.previewbay.com/colins/storage/home/16456012441633959854mission.png" alt="">
                                        
                                                 </div> -->
                                                 @if ($errors->has('img'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('img') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                               
                                                
                                               
                                                <div class="col-md-6 mb-4">
                                                
                                                <fieldset class="form-group">
                                                <label for="basicSelect">Select Status</label>
                                                    <select class="form-select" name="type">
                                                        <option value="Top">Top</option>
                                                        <option value="Bottom">Bottom</option>
                                                    </select>
                                                </fieldset>
                                                @if ($errors->has('type'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('type') }}</strong>
                                     </span>
                                 @endif
                                            </div>

                                            


                                           
 
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit"
                                                        class="btn btn-primary me-1 mb-1">Submit</button>
                                                    <button type="reset"
                                                        class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>



@endsection