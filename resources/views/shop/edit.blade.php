@extends('layout.master')
@section('title') Shop | Notech @endsection
@section('content_title') Shop @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="{{route('admin.shop')}}">Shop</a></li>
<li class="breadcrumb-item active">Edit</li>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit Shop</h4>
            </div>
        <div class="card-body">
            <section id="multiple-column-form">
                <div class="row match-height">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" method="POST" action="{{route('admin.shopupdate',$d->id)}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                        <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Name</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="name" value="{{$d->name}}">
                                                    </div>
                                                    @if ($errors->has('name'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('name') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                                

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Image</label>
                                                        <input type="file" id="country-floating" class="form-control"
                                                            name="img" placeholder="Country">
                                                    </div>
                                                    <div class="mt-3">
                                                    <img width="75" height="75" src="{{asset('/'.$d->img)}}" alt="">
                                        
                                                 </div>
                                                 @if ($errors->has('img'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('img') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Delivery Time</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="time" value="{{$d->time}}">
                                                    </div>
                                                    @if ($errors->has('time'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('time') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Delivery Charge</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="delivery_charge" value="{{$d->delivery_charge}}">
                                                    </div>
                                                    @if ($errors->has('delivery_charge'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('delivery_charge') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Address</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="address" value="{{$d->address}}">
                                                    </div>
                                                    @if ($errors->has('address'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('address') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Whats app</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="number" name="whats_app" value="{{$d->whats_app}}">
                                                    </div>
                                                    @if ($errors->has('whats_app'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('whats_app') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                               
                                                
                                               
                                                <div class="col-md-6 mb-4">
                                                
                                                <fieldset class="form-group">
                                                <label for="basicSelect">Select Status</label>
                                                    <select class="form-select" name="active">
                                                        <option value="1" {{$d->active == '1' ? 'selected' : ''}}>Active</option>
                                                        <option value="0" {{$d->active == '0' ? 'selected' : ''}}>In Active</option>
                                                    </select>
                                                </fieldset>
                                                @if ($errors->has('active'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('active') }}</strong>
                                     </span>
                                 @endif
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                
                                                <fieldset class="form-group">
                                                <label for="basicSelect">Popular</label>
                                                    <select class="form-select" name="popular">
                                                        <option value="1" {{$d->popular == '1' ? 'selected' : ''}}>Yes</option>
                                                        <option value="0" {{$d->popular == '0' ? 'selected' : ''}}>No</option>
                                                    </select>
                                                </fieldset>
                                                @if ($errors->has('popular'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('popular') }}</strong>
                                     </span>
                                 @endif
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                
                                                <fieldset class="form-group">
                                                <label for="basicSelect">Catalogue</label>
                                                @foreach($cat as $c)
                                                <div class="form-check">
                                                <input class="form-check-input" {{in_array($c->id,json_decode($d->catalouge)) ? "checked" : " "}} type="checkbox" name="catalouge[]" value="{{$c->id}}" id="{{$c->id}}" >
                                                <label class="form-check-label" for="{{$c->id}}">
                                                {{$c->name}} - {{$c->type}}
                                                </label>
                                                </div>
                                                @endforeach
                                                @if ($errors->has('catalouge'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('catalouge') }}</strong>
                                     </span>
                                 @endif
                                            </div>

                                            


                                           
 
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit"
                                                        class="btn btn-primary me-1 mb-1">Submit</button>
                                                    <button type="reset"
                                                        class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>



@endsection