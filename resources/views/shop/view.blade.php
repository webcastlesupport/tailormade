@extends('layout.master')
@section('title') Shop | TailorMade @endsection
@section('content_title') Shop @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="#">View Shops</a></li>
<!-- <li class="breadcrumb-item active" aria-current="page">Layout Vertical Navbar</li> -->
@endsection


@section('content')
<section class="section">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-primary rounded-pill" href="{{route('admin.shopadd')}}" style="float: right;">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped" id="table1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Number</th>
                                        <th>Popular</th>
                                        <th>Units</th>
                                        <th>Delivery Charge</th>
                                        <th>Time</th>
                                        <th>Status</th>

                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    @foreach($data as $key=>$d)
                                        <td>{{$key+1}}</td>
                                        
                                        <td>{{$d->name}}</td>
                                        <td>{{$d->address}}</td>
                                        <td>{{$d->whats_app}}</td>
                                        <td><span class="badge bg-success">{{$d->popular == '1' ? 'Yes' : 'No'}}</span></td>
                                        <td><a href="{{route('admin.shopunit',$d->id)}}">Units</a></td>
                                        <td>{{$d->delivery_charge}}</td>
                                        <td>{{$d->time}}</td>
                                        <td><span class="badge bg-success">{{$d->active == '1' ? 'Active' : 'In Active'}}</span></td>
                                       
                                        <td>
                                        <a href="{{route('admin.shopedit',$d->id)}}"><span class="btn btn-primary rounded-pill">Edit</span></a>
                                        </td>
                                    </tr>

                                    @endforeach
                                   
                                    
                                   

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <script src="{{asset('assets/vendors/simple-datatables/simple-datatables.js')}}"></script>
     <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

@endsection