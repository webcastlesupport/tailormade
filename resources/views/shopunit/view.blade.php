@extends('layout.master')
@section('title') Shop | TailorMade @endsection
@section('content_title') Shop Unit @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="{{route('admin.shop')}}">View Shop</a></li>
<li class="breadcrumb-item active" aria-current="page">Unit</li>
@endsection


@section('content')
<section class="section">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-primary rounded-pill" href="{{route('admin.shopunitadd',$id)}}" style="float: right;">Add New</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped" id="table1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Unit</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    @foreach($data as $key=>$d)
                                        <td>{{$key+1}}</td>        
                                        <td>{{$d->unit}}</td>
                                        <td>{{$d->price}}</td>
                                        <td>
                                        <a href="{{route('admin.shopunitedit',$d->id)}}"><span class="btn btn-primary rounded-pill">Edit</span></a>
                                        </td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <script src="{{asset('assets/vendors/simple-datatables/simple-datatables.js')}}"></script>
     <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

@endsection