@extends('layout.master')
@section('title') Shop | Notech @endsection
@section('content_title') Shop @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="{{route('admin.shop')}}">Shop</a></li>
<li class="breadcrumb-item"><a href="{{route('admin.shopunit',$id)}}">Shop Unit</a></li>
<li class="breadcrumb-item active">Add</li>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Add Shop</h4>
            </div>
        <div class="card-body">
            <section id="multiple-column-form">
                <div class="row match-height">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" method="POST" action="{{route('admin.shopunitstore',$id)}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                        <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Unit</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="unit">
                                                    </div>
                                                    @if ($errors->has('unit'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('unit') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                                

                                                
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Price</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="price">
                                                    </div>
                                                    @if ($errors->has('price'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('price') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit"
                                                        class="btn btn-primary me-1 mb-1">Submit</button>
                                                    <button type="reset"
                                                        class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>



@endsection