@extends('layout.master')
@section('title') Users | TailorMade @endsection
@section('content_title') Users @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="#">View Users</a></li>
<!-- <li class="breadcrumb-item active" aria-current="page">Layout Vertical Navbar</li> -->
@endsection


@section('content')
<section class="section">
                    <div class="card">
                        <!-- <div class="card-header">
                            <a class="btn btn-primary rounded-pill" href="{{route('admin.shopadd')}}" style="float: right;">Add New</a>
                        </div> -->
                        <div class="card-body">
                            <table class="table table-striped" id="table1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Address</th>
                                        <th>Number</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    @foreach($data as $key=>$d)
                                        <td>{{$key+1}}</td>
                                        
                                        <td>{{$d->first_name}}</td>
                                        <td>{{$d->last_name}}</td>
                                        <td>{{$d->address}}</td>
                                        <td>{{$d->phone}}</td>
                                        <td>{{$d->email}}</td>
                                        <td><span class="badge bg-success">{{$d->is_active == '1' ? 'Active' : 'In Active'}}</span></td>
                                    </tr>

                                    @endforeach
                                   
                                    
                                   

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <script src="{{asset('assets/vendors/simple-datatables/simple-datatables.js')}}"></script>
     <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

@endsection