@extends('layout.master')
@section('title') Order | TailorMade @endsection
@section('content_title') Order @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="#">View Orders</a></li>
<!-- <li class="breadcrumb-item active" aria-current="page">Layout Vertical Navbar</li> -->
@endsection


@section('content')
<section class="section">
                    <div class="card">
                        
                        <div class="card-body">
                            <table class="table table-striped" id="table1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Id</th>
                                        <th>Booking Date</th>
                                        <th>Delivery Date</th>
                                        <th>User</th>
                                        <th>Shop</th>
                                        <th>Status</th>
                                        <th>Rate</th>
                                        <th>Delivery Charge</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    @foreach($data as $key=>$d)
                                        <td>{{$key+1}}</td>
                                        
                                        <td>{{$d->order_id}}</td>
                                        <td>{{$d->date}}</td>
                                        <td>{{$d->delivery_date}}</td>
                                        <td>{{$d->user->first_name}}</td>
                                        <td>{{$d->shop->name}}</td>
                                        <td><span class="badge bg-success">{{$d->completed == '1' ? 'Payment Received' : 'Payment Not Received'}}</span></td>
                                        <td>{{$d->rate}}</td>
                                        <td>{{$d->shop->delivery_charge}}</td>
                                        <td>{{$d->rate + $d->shop->delivery_charge}}</td>
                                        @if($d->delivery_date == null)
                                        <td>
                                        <a href="{{route('admin.delivery',$d->id)}}"><span class="btn btn-primary rounded-pill">Mark as Delivered</span></a>
                                        </td>
                                        @else
                                        <td>
                                        <a href="#"><span class="btn btn-primary rounded-pill">Delivered</span></a>
                                        </td>
                                        @endif
                                    </tr>

                                    @endforeach
                                   
                                    
                                   

                                   
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
                <script src="{{asset('assets/vendors/simple-datatables/simple-datatables.js')}}"></script>
     <script>
        // Simple Datatable
        let table1 = document.querySelector('#table1');
        let dataTable = new simpleDatatables.DataTable(table1);
    </script>

@endsection