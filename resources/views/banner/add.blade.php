@extends('layout.master')
@section('title') Banner | Notech @endsection
@section('content_title') Banner @endsection
@section('content_subtitle')  @endsection
@section('breadcrumb')  
<li class="breadcrumb-item"><a href="{{route('admin.banner')}}">Banner</a></li>
<li class="breadcrumb-item active">Add</li>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Add Banner</h4>
            </div>
        <div class="card-body">
            <section id="multiple-column-form">
                <div class="row match-height">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form" method="POST" action="{{route('admin.bannerstore')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                        <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Title</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="title">
                                                    </div>
                                                    @if ($errors->has('title'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('title') }}</strong>
                                     </span>
                                 @endif
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Description</label>
                                                        <input type="text" id="last-name-column" class="form-control"
                                                            placeholder="text" name="desc">
                                                    </div>
                                                    @if ($errors->has('desc'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('desc') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                                

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Logo</label>
                                                        <input type="file" id="country-floating" class="form-control"
                                                            name="logo" placeholder="Country">
                                                    </div>
                                                    <!-- <div class="mt-3">
                                                    <img width="75" height="75" src="https://s414.previewbay.com/colins/storage/home/16456012441633959854mission.png" alt="">
                                        
                                                 </div> -->
                                                 @if ($errors->has('logo'))
                                     <span class="error-feedback" role="alert">
                                         <strong>{{ $errors->first('logo') }}</strong>
                                     </span>
                                 @endif
                                                </div>
                                               
                                                
                                               
                                                

                                            


                                           
 
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit"
                                                        class="btn btn-primary me-1 mb-1">Submit</button>
                                                    <button type="reset"
                                                        class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>



@endsection